---
theme : "night"
transition: "fade"
highlighttheme: "monokai"
slidenumber: true
title: "note taking shenanigans"
---

::: block
  ## note taking shenanigans

  my adventure with rust, encryption, and software design.
:::

---

## who am i

most call me dihar {.fragment}

passionate about computers especially software stuff {.fragment}

---

## what is this about?

an encrypted note taking app {.fragment}

software development using rust {.fragment}

---

## the habit

difference between a dreamer and a scientist, is writing it down {.fragment}

---

## take some notes

what does a <u>dihar</u> need for a perfect note taking application? {.fragment}

--

## open source

some call it a religion {.fragment}

<aside class="notes">
Need to be open source because of trust issue with proprietary application screwing up data saves without any legal access to the older version or even if it still exist at all. Also open source program can still live long after the creator of the software is not around anymore.
</aside>

--

## offline first

we are nothing without the internet {.fragment}

<aside class="notes">
Point about not wanting to use cloud only saves because the provider might die someday and that's not a risk I want to cast upon my most sensitive information
</aside>

--

## stable

don't screw up my notes and give me some backups too {.fragment}

--

## encryption

a self signed NDA {.fragment}

<aside class="notes">
everyone deserve privacy and secrets should be protected to the best of our ability
</aside>

---

## some options

taking other application to consideration {.fragment}

--

## obsidian

![Alt text](asset/obsidian.png)

<aside class="notes">
it's proprietary, no portable offline saving, no official support for encryption, no guarantee that my notes are stable.
</aside>

--

## notion

![Alt text](asset/notion.png)

<aside class="notes">
it's proprietary, no portable offline saving, no official support for encryption, no guarantee that my notes are stable.
</aside>

--

## simplenote

![Alt text](asset/simplenote.png)

<aside class="notes">
it's open source, but no portable offline saving, no official support for encryption, no guarantee that my notes are stable..
</aside>

--

## cherrytree

![Alt text](asset/cherrytree.png)

<aside class="notes">
this one checks all the boxes because it's open source, it stores the notes on a single sqlite file, it supports encryption with passwords, and the save file is known to be stable with a generous amount of backup functionality.
</aside>

--

## miscellaneous

For any other suggestion, please contact me later @Dihar.

---

## problem statement

the quirk of cherrytree is annoying at best and deal breaker at worst {.fragment}

--

## second class citizen

note encryption felt like an afterthought for cherrytree {.fragment}

<aside class="notes">
an unprotected copy is extracted to a temporary -folder of the filesystem; this copy is removed when you close cherrytree. slow load time for big notes even if we are only opening the relatively small, text only notes.
</aside>

--

## long saving time

after some digging, it loads everything to memory and re-encrypt everything when saving {.fragment}

--

## old user interface

the ux is not the best around {.fragment}

---

## enter,
## treedome {.fragment}

---

![Alt text](asset/treedome.png)

---

## ticking some boxes

create something that satisfy your need {.fragment}

---

## what I have learned

treedome as a project is a great tool for me to broaden my horizon {.fragment}

--

## embedded file storage is hard

many options but none of them beat the classic {.fragment}

<aside class="notes">
There are many option for embedded database in rust but I've listed some of the promising options beside the classic. We'll talk about what the classic is later.
</aside>

--

## ReDB

clunky {.fragment}

very large size when in disk {.fragment}

doesn't vacuum that well {.fragment}

<aside class="notes">
First db that was tried. Clunky when being used because of the massive boilerplate to initiate every single table if it doesn't exist yet which is a PITA.
</aside>

--

## BonsaiDB

small when persisted to disk {.fragment}

comfortable to use in rust {.fragment}

unfortunately the file format is not stable yet {.fragment}

<aside class="notes">
</aside>

--

## Persy

the API looks very immature {.fragment}

unique key is not sane enough because it's only using integer {.fragment}

still have no idea where the vacuum/compact methods are {.fragment}

<aside class="notes">
</aside>

--

## SQLite

no introduction needed {.fragment}

<aside class="notes">
battle tested. it only use a single file. sql is familiar and the library of choice is plenty. can use many already common tools to debug the sqlite file.
</aside>

--

## encryption is hard

password based encryption is hard to get right {.fragment}

<aside class="notes">
TODO: rage dan kenapa saving itu bisa sampe satu detik
</aside>

--

## a brief introduction to password based encryption

require the user to set a good password {.fragment}

put the password into an expensive function with some salt {.fragment}

wait for a moment {.fragment}

your **Password-Based Key Derivative** is ready {.fragment}


<aside class="notes">
TODO: rage dan kenapa saving itu bisa sampe satu detik
</aside>

--

## rage against bad actor

library to encrypt a file {.fragment}

it is slow {.fragment}

can't reuse the same private key for different data {.fragment}

<aside class="notes">
TODO: rage dan kenapa saving itu bisa sampe satu detik
</aside>

--

## rolling out (a part of) your encryption

can be used in our usecase {.fragment}

made the cipher reusable {.fragment}

faster because cipher can be used more than once {.fragment}

xchacha20 poly1305 with random nonces {.fragment}


<aside class="notes">
don't talk about nonces and shit, that will take forever. You're using xchacha and the private key is derived by a secure password using scrypt.
</aside>

--

## let the compiler think

thank god for rust's static typing and checking {.fragment}

<aside class="notes">
TODO: mengenai tipe struct, enum, refactoring yang gampang dan menyelamatkan, linting, etc.
</aside>

--

## web based technology is time saving

![Alt text](asset/patrick-likes-web-tech.png)

---

## check it out yourself!

<img class="fragment" width="400" height="400" data-src="/asset/treedome-qrcode.svg" />

---

## thank you!

https://codeberg.org/solver-orgz/treedome