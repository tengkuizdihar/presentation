---
theme : "night"
transition: "fade"
highlighttheme: "monokai"
slidenumber: true
title: "licensing shenanigans"
---

::: block
  ## Licensing Shenanigans

  by tengkuizdihar@gmail.com
:::

---

## Profile

- tengkuizdihar@gmail.com {.fragment}
- active professional software engineer {.fragment}
- interest in software as a tool and culture {.fragment}
- Am Not A Lawyer {.fragment}

---

## About

- demistifying common licensing for software {.fragment}
- the meaning of open source {.fragment}
- common misconception of open source {.fragment}
- an evolution in open source {.fragment}

---

## Common Licensing

- a software is an intellectual property {.fragment}
- an intellectual property need a license {.fragment}
- a license control the user, not just the software {.fragment}

---

## Proprietary

- creator/copyright owner controls software usage
- primarily used by for-profit companies

--

## Advantage

- monopoly in usage
- discouraging and eliminating copy-cats
- creating a barrier of entry for business competitors
- locking in customer to our service

<aside class="notes">
for point 4 crowdstrike, a monopoly in the cloud infrastructure security, its vendor lock ins, and the diminishing spirit of competition between rivals.
</aside>

--

## Critiques

- discrimination against user's action
- propogating a trend of software as a lease, not owned
- commonly not transparent in its implementation
- vendor lock-in is directly against right-to-repair

<aside class="notes">
control the user, remind them of Disney v Jeffrey Piccolo, where Disney argues Mr. Piccolo must use Disney Internal Court (arbitration) because he signed for Disney Plus Trial. Disney did waive it later though, after a huge backlash https://www.theverge.com/2024/8/20/24224277/disney-wrongful-death-lawsuit-waiving-arbitration.
</aside>

---

## Open Source

<!-- TODO: the meaning of open source -->

--

## Example

<!-- TODO: impactful open source also add treedome lmao -->

--

## Definition - Part 1

<!-- Open Source Rights based on FSF definition -->

1. Free Redistribution
2. Inclusion of Source Code
3. Allowing Derived Works
4. Integrity of The Author’s Source Code and Allowing Patch
5. No Discrimination Against Persons or Groups

--

## Definiton - Part 2

6. No Discrimination Against Fields of Endeavor
7. Distribution of License
8. License Must Not Be Specific to a Product
9. License Must Not Restrict Other Software
10. License Must Be Technology-Neutral

--

## Misconception

<!-- TODO: Source Available != Open Source -->
<!-- TODO: Open Source is not secure -->
<!-- TODO: Open source is not commercially viable -->
<!-- TODO: Open source is not user friendly -->
<!-- TODO: Open source is difficult to maintain -->
<!-- TODO: Open source has bad support -->

---

## Evolution in Open Source Philosophy

<!-- TODO: kasih paham tentang copyleft -->

---

## References

Search for licensing-shenanigans.md in QR Code

<img src="/asset/presentation-url-qrcode.svg" width="400" height="400">

<!-- 
- Disney v J. Piccolo Arbitration https://www.theverge.com/2024/8/20/24224277/disney-wrongful-death-lawsuit-waiving-arbitration
- Open Source Definition by Open Source Initiative https://opensource.org/osd
-->