---
theme : "night"
transition: "slide"
highlightTheme: "monokai"
slideNumber: true
title: "Corroded By Rust"
---

::: block
  ## Oxidized By Rust

  A surface level explanation of Rust
  
  By Tengku Izdihar
:::

---

## How I Found Rust

- Tired of language that are slow on runtime {.fragment}
- Can't sleep because of unsolvable pitfalls such as null {.fragment}
- Need a better and proper definition for generic and interfaces {.fragment}
- I ~~hate~~ dislike inheritance because I think composition is superior {.fragment}

---

## What To Expect

- characteristic overview of Rust {.fragment}
- use cases of Rust {.fragment}
- benefit of Rust {.fragment}
- experience of using Rust {.fragment}

---

## What is Rust?

<img src="/asset/ferris.png" width="600" height="400">

--

- Statically typed with type inference {.fragment}
- Compiled {.fragment}
- No GC {.fragment}
- Doesn't have null {.fragment}
- Mostly correct {.fragment}
- Great learning resources such as "Rust By Example" {.fragment}
- Comes with formatter, build systems, package management, and more. {.fragment}
- Most loved language (stackoverflow poll) {.fragment}

--

## Where is it used?

- Discord Read State System, incidentally switching from Go {.fragment}
- Dropbox Sync Engine which produced only a limited memory errors {.fragment}
- Mozilla for its CSS engine, they also made this language {.fragment}
- Cloudflare ultra high performance HTTP Proxy, called Pingora {.fragment}

--

```rust
#[derive(Debug)]
struct Person<'a> {
    name: &'a str,
    age: u8
}

fn main() {
    let name = "Peter";
    let age = 27;
    let peter = Person { name, age };

    // Pretty print
    println!("{:#?}", peter);
}

```

---

## The Characteristics of Rust

How it works, accompanied by examples.

--

#### How To Compile

```bash
cargo build
```

or

```bash
cargo build --release
```

--

#### Project Structure

```
root
├── Cargo.toml         // equivalent to packages.json
├── Cargo.lock         // equivalent to packages-lock.json
└── src
    └── main.rs
```

--

#### Immutability By Default - 1

```rust
struct People {
    age: u64
}

fn main() {
    let dihar = People {
        age: 10
    };
    dihar.age = 20; // this will not compile
}
```

--

#### Immutability By Default - 2

```rust
struct People {
    age: u64
}

fn main() {
    let mut dihar = People {
        age: 10
    };
    dihar.age = 20; // this will compile
}
```

--

#### Commonly Used Types

- unsigned integers such as `u32`
- signed integers such as `i8`
- floating point such as `f64`
- heap allocated types using generic by `Box<T>`
- `&'a str` and its heap allocated variation `String`

--

#### Custom Types - 1

```rust
// regular struct
#[derive(Debug)]
struct Person {
    name: String,
    age: u8,
}
```


--

#### Custom Types - 2

```rust
// unit struct
struct Unit;
```


--

#### Custom Types - 3

```rust
// tuple struct
struct Coordinate(f64, f64, f64);
```


--

#### Custom Types - 4

```rust
// struct inside another struct
struct Player {
    position: Coordinate,
    health: f64,
}
```

--

#### Custom Types - 5

```rust
// null doesn't exist in rust so we use option enums
// either it has Some(value)
// or None at all
pub enum Option<T> {
    None,
    Some(T),
}
```

--

#### Type Inference - 1

```rust
let foo = Player {
    name: "dihar".to_string(),
    age: 23
};
```

--

#### Type Inference - 2

```rust
let foo: Player = Player {
    name: "dihar".to_string(),
    age: 23
};
```

--

#### Generic - 1

```rust
struct Container<T>(T);

...

let string_container = Container("String".to_string());
let int_container = Container(10i32);
```

--

#### Generic - 2

```rust
struct Container(String);
struct Container(i32);

...

let string_container = Container("String".to_string());
let int_container = Container(10i32);
```

--

#### Traits and Methods - 1

```rust
struct Sheep { naked: bool, name: &'static str }
trait Animal {
    // `Self` refers to the implementor type.
    fn new(name: &'static str) -> Self;

    // Traits can provide default method definitions.
    fn talk(&self) {
        println!("{} says {}", self.name(), self.noise());
    }
}
```

--

#### Traits and Methods - 2

```rust
impl Sheep {
    fn is_naked(&self) -> bool {
        self.naked
    }

    fn shear(&mut self) {
        if self.is_naked() {
            println!("{} is already naked...", self.name());
        } else {
            println!("{} gets a haircut!", self.name);

            self.naked = true;
        }
    }
}
```

--

#### Traits and Methods - 3

```rust
// Implement the `Animal` trait for `Sheep`.
impl Animal for Sheep {
    // `Self` is the implementor type: `Sheep`.
    fn new(name: &'static str) -> Sheep {
        Sheep { name: name, naked: false }
    }

    // Default trait methods can be overridden.
    fn talk(&self) {
        // For example, we can add some quiet contemplation.
        println!("{} pauses... {}", self.name, self.noise());
    }
}
```

--

#### Not Covered

- Modules and Project Configuration {.fragment}
- Advance Enums and Sum Type {.fragment}
- Pattern Matching {.fragment}
- Ownership System, Borrowing, Lifetimes {.fragment}
- Macros, Derive Macros, and Attribute {.fragment}
- Error Handling {.fragment}
- Advanced Generic {.fragment}
- Parallelism and Async {.fragment}
- Unsafe Operation {.fragment}
- And More! {.fragment}

---

#### My Thoughts On Rust

- It's a wonderful language and we should adopt it! {.fragment}
- The compiler corrects a lot of mistake made by the programmer! {.fragment}
- No nulls, you can sleep well at night! {.fragment}
- But... {.fragment}
- It's slow to compile {.fragment}

---

#### My Thoughts On Rust

- There's already an abundant of choice of libraries out there! {.fragment}
- But... {.fragment}
- Not all of them are matured by the test of time {.fragment}

---

#### My Thoughts On Rust

- There's a lot of good paradigm that encourages programmer to write good code! {.fragment}
- But... {.fragment}
- You need to learn to befriend the compiler {.fragment}
- Steep learning curve, like C++ to a Python programmer {.fragment}

---

#### My Thoughts On Rust

- Recommended for high performance and applications {.fragment}
- For use cases where unhandled errors lead to a catastrophe {.fragment}
- To minimize operational cost (https://programming-language-benchmarks.vercel.app/go-vs-rust) {.fragment}
- Should learn from practices of other companies to integrate it {.fragment}
- Has a bright future ahead of it {.fragment}

---

#### THANK YOU!

<img src="/asset/presentation-url-qrcode.svg" width="400" height="400">

---

#### Contacts

- tengkuizdihar@gmail.com
- [@tengkuizdihar:matrix.org](https://matrix.to/#/@tengkuizdihar:matrix.org)
- [mastodon.gamedev.place/@tengkuizdihar](https://mastodon.gamedev.place/@tengkuizdihar)
- codeberg.org/tengkuizdihar
- scream my name thrice in front of a mirror
