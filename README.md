# What Is This?

This is a repository filled with presentation files that I use for public forums. One markdown file means one presentation. Powered by [revealjs](https://revealjs.com/).

# How To Present

1. Prepare an installation of VSCode or VSCodium.
2. Search for an extension called [vscode-reveal](https://github.com/evilz/vscode-reveal).
3. Open the markdown file of the presentation of your chosing.
4. Press `ctrl+shift+p` and search for `Revealjs: Open In Browser`.